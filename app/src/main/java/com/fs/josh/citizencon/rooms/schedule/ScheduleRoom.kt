package com.fs.josh.citizencon.rooms.schedule

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "schedule")
class ScheduleRoom {
    @PrimaryKey var id: Int = 0
    var title: String = ""
    var description: String = ""
    var start: String = ""
    var length: Double = 0.0
    var speakers: ArrayList<String> = arrayListOf()
    var isAttending: Boolean = false
    var stage: Int = 0
}