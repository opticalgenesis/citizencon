package com.fs.josh.citizencon.ui.schedule

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

// TODO rename this to ScheduleStageTwo
class SchedulePageTwo : Fragment() {

    private lateinit var vm: ScheduleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("PAGER_TAG", "Schedule Two")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        vm = ViewModelProviders.of(this)[ScheduleViewModel::class.java]
        vm.getBasicEvents("stage_two").observe(this, Observer {
            it.forEach { item ->
                Log.d("STAGE_TWO", "Item: ${item.title}")
            }
        })
    }
}