package com.fs.josh.citizencon.rooms.schedule

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface ScheduleDao {
    @Query("SELECT * FROM schedule")
    fun getAllEvents(): List<ScheduleRoom>

    @Insert
    fun addEvent(event: ScheduleRoom)

    @Update
    fun updateAttendance(b: Boolean)
}