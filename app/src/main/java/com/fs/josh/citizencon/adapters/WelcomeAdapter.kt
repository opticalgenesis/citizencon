package com.fs.josh.citizencon.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.TextureView
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.fs.josh.citizencon.R
import com.fs.josh.citizencon.ui.schedule.ScheduleFragment

class WelcomeAdapter(private val ctx: Context) : RecyclerView.Adapter<WelcomeAdapter.ItemHolder>() {

    private val list = arrayListOf("Schedule", "Local", "About")
    private val fragments = arrayListOf(R.id.scheduleFragment)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder = ItemHolder(parent)

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bind(list[position])
        holder.tv.setOnClickListener { holder.tv.findNavController().navigate(fragments[position]) }
    }

    override fun getItemCount(): Int = list.size

    inner class ItemHolder(vg: ViewGroup) : RecyclerView.ViewHolder(LayoutInflater.from(vg.context).inflate(R.layout.welcome_item_holder, vg, false)) {
        val tv = itemView.findViewById<TextView>(R.id.welcome_item_text)

        internal fun bind(event: String) {
            tv.text = event
        }
    }
}