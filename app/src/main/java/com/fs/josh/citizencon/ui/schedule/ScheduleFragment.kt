package com.fs.josh.citizencon.ui.schedule

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import androidx.viewpager.widget.ViewPager
import com.fs.josh.citizencon.R
import com.fs.josh.citizencon.adapters.ScheduleAdapter
import com.fs.josh.citizencon.rooms.schedule.ScheduleDatabase
import com.fs.josh.citizencon.rooms.schedule.ScheduleRoom
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class ScheduleFragment : Fragment() {

    private val auth by lazy { FirebaseAuth.getInstance() }
    private var user: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        user = auth.currentUser
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.schedule_fragment, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val vp = view?.findViewById<ViewPager>(R.id.vp)
        vp?.adapter = SchedulePagerAdapter(requireFragmentManager())
        val tl = view?.findViewById<TabLayout>(R.id.fragment_schedule_tab_layout)
        tl?.setupWithViewPager(vp)
    }
}