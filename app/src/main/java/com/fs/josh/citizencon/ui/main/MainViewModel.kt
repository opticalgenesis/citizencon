package com.fs.josh.citizencon.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore

class MainViewModel : ViewModel() {
    private val db by lazy { FirebaseFirestore.getInstance() }

    private lateinit var debug: MutableLiveData<String>

    fun getDebug(): LiveData<String> {
        if (!::debug.isInitialized) {
            debug = MutableLiveData()
            loadDebug()
        }
        return debug
    }

    private fun loadDebug() {
        db.collection("debug").get()
                .addOnCompleteListener { t ->
                    Log.d("OBS_TAG", "NEw data")
                    if (t.isSuccessful) {
                        t.result?.forEach {
                            it.reference.addSnapshotListener { documentSnapshot, firebaseFirestoreException ->
                                if (firebaseFirestoreException != null) {
                                    Log.e("OBS_TAG", "FStoreError")
                                } else {
                                    if (documentSnapshot != null && documentSnapshot.exists()) {
                                        val d = documentSnapshot.data
                                        debug.postValue(d?.get("hello") as String)
                                    }
                                }
                            }
                        }
                    }
                }
    }
}
