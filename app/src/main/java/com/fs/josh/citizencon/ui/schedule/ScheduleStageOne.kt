package com.fs.josh.citizencon.ui.schedule

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fs.josh.citizencon.R
import com.fs.josh.citizencon.adapters.ScheduleAdapter
import com.fs.josh.citizencon.rooms.schedule.ScheduleRoom

class ScheduleStageOne : Fragment() {

    private lateinit var rv: RecyclerView
    private var adapter: ScheduleAdapter? = null
    private lateinit var vm: ScheduleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("PAGER_TAG", "Schedule One")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.schedule_stage_one, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv = view.findViewById(R.id.fragment_stage_one_schedule_recycler)
        rv.layoutManager = LinearLayoutManager(requireContext())
        adapter = ScheduleAdapter(requireContext())
        rv.adapter = adapter
        adapter?.hasStableIds()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        vm = ViewModelProviders.of(this)[ScheduleViewModel::class.java]
        vm.getBasicEvents("stage_one").observe(this, Observer {
            adapter?.updateList(it)
        })
    }
}