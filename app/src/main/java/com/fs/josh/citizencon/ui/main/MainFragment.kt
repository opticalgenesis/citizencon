package com.fs.josh.citizencon.ui.main

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.fs.josh.citizencon.R
import com.fs.josh.citizencon.ui.local.LocalViewModel
import com.google.android.gms.location.places.GeoDataClient
import com.google.android.gms.location.places.PlaceDetectionClient
import com.google.android.gms.location.places.Places
import java.lang.StringBuilder
import java.util.concurrent.TimeUnit

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sb = StringBuilder()
        sb.apply {
            append("Citizen Con 2949 in ")
            append("LOCATION ")
            append("begins in: ")
        }
        val welcome = view.findViewById<TextView>(R.id.fragment_main_welcome)
        welcome.text = "${welcome.text} LowPolyButt!"
        view.findViewById<TextView>(R.id.fragment_main_next_details).text = sb.toString()
        val tickerTextView = view.findViewById<TextView>(R.id.fragment_main_ticker)
        val vm = ViewModelProviders.of(this)[LocalViewModel::class.java]
        vm.getTimeLeft().observe(this, Observer {
            tickerTextView.text = it
        })
    }
}
